import javafx.fxml.FXML;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

public class Controller
{
    @FXML
    private javafx.scene.control.Button browseButton;
    @FXML
    private javafx.scene.control.Button exitButton;
    @FXML
    private javafx.scene.control.TextField fileDirectoryDisplay;
    @FXML
    private javafx.scene.control.TextField md5Display;
    @FXML
    private javafx.scene.control.TextField sha1Display;
    @FXML
    private javafx.scene.control.TextField sha256Display;
    @FXML
    private javafx.scene.control.TextField sha512Display;

    public void browseButtonControl() throws Exception
    {
        Stage stage = (Stage) browseButton.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Browse");
        File file = fileChooser.showOpenDialog(stage);

        if (file != null)
        {
            fileDirectoryDisplay.setText(file.getAbsolutePath());
            md5Display.setText(getHash(file, "MD5"));
            sha1Display.setText(getHash(file, "SHA1"));
            sha256Display.setText(getHash(file, "SHA-256"));
            sha512Display.setText(getHash(file, "SHA-512"));
        }
    }
    public void md5CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(md5Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha1CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha1Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha256CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha256Display.getText());
        clipboard.setContents(selection, null);
    }
    public void sha512CopyButtonControl()
    {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Clipboard clipboard = toolkit.getSystemClipboard();
        StringSelection selection = new StringSelection(sha512Display.getText());
        clipboard.setContents(selection, null);
    }
    public void clearButtonControl()
    {
        fileDirectoryDisplay.setText("");
        md5Display.setText("");
        sha1Display.setText("");
        sha256Display.setText("");
        sha512Display.setText("");
    }
    public void exitButtonControl()
    {
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }
    private String getHash(File filePath, String hashType) throws Exception
    {
        String hash;
        MessageDigest messageDigest = MessageDigest.getInstance(hashType);
        FileInputStream fileInputStream = new FileInputStream(filePath.toString());
        byte[] dataBytes = new byte[1024];
        int count;

        while ((count = fileInputStream.read(dataBytes)) != -1)
        {
            messageDigest.update(dataBytes, 0, count);
        }

        byte[] messageDigestBytes = messageDigest.digest();

        StringBuilder stringBuilder = new StringBuilder();

        for (int index = 0; index < messageDigestBytes.length; index++)
        {
            stringBuilder.append(Integer.toString((messageDigestBytes[index] & 0xff) + 0x100, 16).substring(1));
        }

        StringBuilder hexString = new StringBuilder();

        for (int index = 0; index < messageDigestBytes.length; index++)
        {
            String hex = Integer.toHexString(0xff & messageDigestBytes[index]);

            if(hex.length() == 1)
            {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        hash = hexString.toString().toUpperCase();

        return hash;
    }
}
